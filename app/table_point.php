<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;

class table_point extends Model
{
    use Authenticatable;
    protected $table = 'table_point';
    protected $primaryKey = 'id_point';
}