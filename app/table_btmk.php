<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;

class table_btmk extends Model
{
    use Authenticatable;
    protected $table = 'table_btmk';
    protected $primaryKey = 'NO_AGGR';
}