<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;

class table_api_key extends Model
{
    use Authenticatable;
    protected $table = 'table_api_key';
    protected $primaryKey = 'id';
}