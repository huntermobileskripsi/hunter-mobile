<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;

class table_draft extends Model
{
    use Authenticatable;
    protected $table = 'table_draft';
    protected $primaryKey = 'id_draft';
    protected $fillable = ['no_polisi','pict_tumbnail','location','desc_location','desc_unit','dt_added','user_added','id_user','status','pict_tumbnail2',
        'nama_lengkap'];
}