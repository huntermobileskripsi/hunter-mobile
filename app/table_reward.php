<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;

class table_reward extends Model
{
    use Authenticatable;
    protected $table = 'table_reward';
    protected $primaryKey = 'id_reward';
}