<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;

class table_user extends Model
{
    use Authenticatable;
    protected $table = 'table_user';
    protected $primaryKey = 'id_user';
    protected $fillable = ['username','password','nama_lengkap','alamat','no_rek','jenis_kelamin','flag_akrif','email','kode','dv','flag_active'
        ,'kode_forgot','no_hp','nama_bank','date_login','date_register','date_activated','remember_token','updated_at','role'];
}