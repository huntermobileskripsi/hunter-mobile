<?php


namespace App\Http\Controllers\API;


use App\table_api_key;
use App\table_log;
use App\table_suratkuasa;
use App\table_user;
use Illuminate\Http\Request;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use Illuminate\Support\Facades\Auth;
use Validator;
use Hash;



class getAllAktifSK extends APIBaseController
{
    public function log(Request $request)
    {
        $date = date('Y-m-d H:m:s');
        if (table_api_key::where('api_key', '=', $request->get('apiKey'))->exists()) {
            $validator = Validator::make($request->all(), [
                'id_user' => 'required',

            ]);
            if ($validator->fails()) {
                $id = $request->get('id_user');
                table_log::insert(array(
                    'ws_name' => 'getAllAktifSK.php',
                    'message_log' => 'Get all sk aktig gagal',
                    'created_at' => $date,
                    'id_user' => $id,
                ));
                return response()->json(['error' => $validator->errors()], 401);
            }
            else {
                $id = $request->get('id_user');
                $data = table_suratkuasa::join('table_btmk','table_suratkuasa.id_btmk','=','table_btmk.id_btmk')
                    ->join('table_user','table_suratkuasa.id_user','=','table_user.id_user')
                    ->select('table_suratkuasa.id_suratkuasa','table_btmk.NO_CAR_POLICE','table_btmk.DESC_VEHICLE_BRAND',
                        'table_btmk.DESC_VEHICLE_TYPE','table_btmk.COLOR','table_suratkuasa.desc_sp','table_suratkuasa.area',
                        'table_user.nama_lengkap','table_suratkuasa.created_at')
                    ->where('table_suratkuasa.id_user','=',$id)
                    ->where('table_suratkuasa.is_deleted','=',0)
                    ->get();
                if(isset($data)){
                    table_log::insert(array(
                        'ws_name' => 'getAllAktifSK.php',
                        'message_log' => 'Get all sk aktif berhasil',
                        'created_at' => $date,
                        'id_user' => $id,
                    ));
                    return $this->sendResponse($data,'Log berhasil Di-input');
                }
                else{
                    table_log::insert(array(
                        'ws_name' => 'getAllAktifSK.php',
                        'message_log' => 'Get all sk aktif gagal',
                        'created_at' => $date,
                        'id_user' => $id,
                    ));
                    return $this->sendError('Log gagal Di-input');
                }
            }
        }
        else{
            table_log::insert(array(
                'ws_name' => 'getAllAktifSK.php',
                'message_log' => 'Cek gagal',
                'created_at' => $date,
                'id_user' => '0',
            ));
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }
}