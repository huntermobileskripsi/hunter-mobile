<?php


namespace App\Http\Controllers\API;


use App\table_api_key;
use App\table_bid_lsk;
use App\table_detail_lelang_sk;
use App\table_lelang_sk;
use App\table_log;
use App\table_user;
use Illuminate\Http\Request;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use Validator;
use Hash;



class doTawarLSK extends APIBaseController
{
    public function Store(Request $request)
    {
        $date = date('Y-m-d H:m:s');
        if (table_api_key::where('api_key', '=', $request->get('apiKey'))->exists()) {
            $input = $request->all();
            $validator = Validator::make($input, [
                'id_user' => 'required',
                'lowest_bto_lsk' => 'required',
                'id_lsk' => 'required'
            ]);
            $id_user = $request->get('id_user');
            $id_lsk = $request->get('id_lsk');
            if ($validator->fails()) {
                table_log::insert(array(
                    'ws_name' => 'doTawarLSK.php',
                    'message_log' => 'Data Tidak Sesuai',
                    'created_at' => $date,
                    'id_user' => $id_user,
                ));
                return $this->sendError('Validation Error.', $validator->errors());
            }
            else {
                $post1 = table_detail_lelang_sk::where('id_lsk','=',$id_lsk)
                    ->where('id_reg_mitra_dlsk','=',$id_user)
                    ->first();
                $post1->lowest_bto_dlsk = $input['lowest_bto_lsk'];
                $post1->updated_at = $date;
                $post1->updated_by = $id_user;
                $post1->save();

                $post = table_lelang_sk::where('id_lsk','=',$id_lsk)
                    ->first();
                $post->lowest_bto_lsk = $input['lowest_bto_lsk'];
                $post->id_user_lowest_lsk = $id_user;
                $post->updated_at = $date;
                $post->updated_by = $id_user;
                $post->save();

//                $post2 = new table_bid_lsk();
//                $post2->id_detail_lsk = $input['id_lsk'];
//                $post2->id_reg_mitra_dlsk = $id_user;
//                $post2->jumlah_bid_dlsk = $input['lowest_bto_lsk'];
//                $post2->created_at = $date;
//                $post2->created_by = $id_user;
//                $post2->save();

                if($post->save() && $post1->save()) {
                    $post2 = new table_bid_lsk();
                    $post2->id_detail_lsk = $post1->id_detail_lsk;
                    $post2->id_reg_mitra_dlsk = $id_user;
                    $post2->jumlah_bid_dlsk = $input['lowest_bto_lsk'];
                    $post2->created_at = $date;
                    $post2->created_by = $id_user;
                    $post2->save();
                    table_log::insert(array(
                        'ws_name' => 'lelangsk.php/doBidLelangSK',
                        'message_log' => 'Data Sesuai',
                        'created_at' => $date,
                        'id_user' => $id_user,
                    ));
                    return $this->sendResponse1('Log Berhasil Di-input / Data Sesuai');
                }
                else{
                    table_log::insert(array(
                        'ws_name' => 'doTawarLSK.php',
                        'message_log' => 'Data Tidak Sesuai',
                        'created_at' => $date,
                        'id_user' => $id_user,
                    ));
                    return $this->sendError('Log Berhasil Di-input / Data Tidak Sesuai');
                }
            }
        }
        else{
            table_log::insert(array(
                'ws_name' => 'doTawarLSK.php',
                'message_log' => 'Update Password User Gagal',
                'created_at' => $date,
                'id_user' => '0',
            ));
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }
}