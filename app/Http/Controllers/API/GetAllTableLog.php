<?php


namespace App\Http\Controllers\API;


use App\table_api_key;
use App\table_log;
use App\table_user;
use Illuminate\Http\Request;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use Validator;
use Hash;



class GetAllTableLog extends APIBaseController
{
    public function Index()
    {
        $post = table_log::orderby('dt_added','desc')->take(10)->get();
        return $this->sendResponse($post, 'Data berhasil di ambil');
    }


}