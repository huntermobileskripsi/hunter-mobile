<?php

namespace App\Http\Controllers\API;

use App\Mail\OTPAktivasi;
use App\Mail\SendEmail;
use App\table_api_key;
use App\table_log;
use App\table_user;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use carbon\carbon;

class doDaftar extends APIBaseController
{
    public $successStatus = 200;
     /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $date = date('Y-m-d H:m:s');
        if(table_api_key::where('api_key','=', $request->get('apiKey'))->exists()) {
            $validator = Validator::make($request->all(), [
                'nama_lengkap' => 'required',
                'email' => 'required|email',
                'alamat' => '',
                'no_hp' => 'required',
                'no_ktp' => 'required',
                'role' => 'required',
                'password' => 'required',

            ]);
            if ($validator->fails()) {
                table_log::insert(array(
                    'ws_name' => 'doDaftar.php',
                    'message_log' => 'Register gagal',
                    'created_at' => $date,
                    'id_user' => '0',
                ));
                return response()->json(['error' => $validator->errors()], 401);
            }
            else {
                if(User::where('email','=',$request->get('email'))
                    ->where('is_deleted','!=','1')
                    ->exists())
                {
                    return $this->sendError('Email sudah terdaftar');
//                    return $this->sendError('The email has already been taken.');
                }
                else {
                    if (User::where('no_hp', '=', $request->get('no_hp'))
                        ->where('is_deleted','!=','1')
                        ->exists()) {
                        return $this->sendError('No HP sudah terdaftar');
//                        return $this->sendError('The no hp has already been taken.');
                    } else {
                        $input = $request->all();
                        $input['password'] = bcrypt($input['password']);
                        $input['nama_lengkap'] = strtoupper($input['nama_lengkap']);
                        $post = User::create($input);
                        $post->date_register = $date;
                        $post->is_deleted = '2';
                        $post->save();
                        do {
                            $str = str_random(4);
                        } while (table_user::where('otp', '=', $str)->exists());
                        table_user::where('id_user', '=', $post->id_user)
                            ->update(array(
                                'otp' => strtoupper($str),
                            ));
                        $user = User::where('id_user', '=', $post->id_user)->first();
                        \Mail::to($user)->send(new OTPAktivasi($user));
                        table_log::insert(array(
                            'ws_name' => 'doDaftar.php',
                            'message_log' => 'Register berhasil',
                            'created_at' => $date,
                            'id_user' => '0',
                        ));
                        $success['id_user'] = $post->id_user;

                        return $this->sendResponse($success, 'Data berhasil');
                    }
                }
            }
        }
        else{
            table_log::insert(array(
                'ws_name' => 'doDaftar.php',
                'message_log' => 'Register gagal',
                'created_at' => $date,
                'id_user' => '0',
            ));
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }
}
