<?php

namespace App\Http\Controllers\API;

use App\Mail\SendEmail;
use App\table_api_key;
use App\table_log;
use App\table_suratkuasa;
use App\table_user;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use carbon\carbon;
use Illuminate\Support\Facades\DB;

class doLogOut extends APIBaseController
{
    public $successStatus = 200;


    public function logout(Request $request){
        $date = date('Y-m-d H:m:s');
        $id = Auth::id();
        if (table_api_key::where('api_key', '=', $request->get('apiKey'))->exists()) {
            if (Auth::user()) {
                $accessToken = Auth::user()->token();

                DB::table('oauth_refresh_tokens')
                    ->where('access_token_id', $accessToken->id)
                    ->update([
                        'revoked' => true
                    ]);

                $accessToken->revoke();

                table_log::insert(array(
                    'ws_name' => 'doLogOut.php',
                    'message_log' => 'Logout berhasil',
                    'created_at' => $date,
                    'id_user' => $id,
                ));

                return $this->sendResponse1('Logout Berhasil');
            } else {
                table_log::insert(array(
                    'ws_name' => 'doLogOut.php',
                    'message_log' => 'Logout gagal',
                    'created_at' => $date,
                    'id_user' => $id,
                ));
                return $this->sendError('Logout gagal');
            }
        }
        else{
            table_log::insert(array(
                'ws_name' => 'doLogOut.php',
                'message_log' => 'Logout gagal',
                'created_at' => $date,
                'id_user' => $id,
            ));
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }
}
