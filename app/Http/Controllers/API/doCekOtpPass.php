<?php


namespace App\Http\Controllers\API;


use App\table_api_key;
use App\table_log;
use App\table_user;
use Illuminate\Http\Request;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use Validator;
use Hash;



class doCekOtpPass extends APIBaseController
{
    public function Store(Request $request)
    {
        $date = date('Y-m-d H:m:s');
        if (table_api_key::where('api_key', '=', $request->get('apiKey'))->exists()) {
            $input = $request->all();
            $validator = Validator::make($input, [
                'otp' => 'required',
            ]);
            if ($validator->fails()) {
                table_log::insert(array(
                    'ws_name' => 'doPerbaruiKataSandi.php',
                    'message_log' => 'Password gagal diganti',
                    'created_at' => $date,
                    'id_user' => '0',
                ));
                return $this->sendError('Validation Error.', $validator->errors());
            }
            else {
                $post = table_user::where('otp','=',$request->get('otp'))->first();
                if (is_null($post)) {
                    table_log::insert(array(
                        'ws_name' => 'getCekLupaKataSandi.php',
                        'message_log' => 'Cek berhasil',
                        'created_at' => $date,
                        'id_user' => '0',
                    ));
                    return $this->sendError('Data tidak ditemukan');
                }
                else{

                    $success['id_user'] = $post->id_user;
                    table_log::insert(array(
                        'ws_name' => 'doPerbaruiKataSandi.php',
                        'message_log' => 'Password berhasil diganti',
                        'created_at' => $date,
                        'id_user' => $post->id_user,
                    ));
                    $del = table_user::find($post->id_user);
                    $del->otp = null;
                    $del->save();
                    return $this->sendResponse($success, 'Data sesuai');
                }
            }
        }
        else{
            table_log::insert(array(
                'ws_name' => 'doPerbaruiKataSandi.php',
                'message_log' => 'Password gagal diganti',
                'created_at' => $date,
                'id_user' => '0',
            ));
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }


}