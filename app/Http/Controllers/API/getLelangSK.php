<?php


namespace App\Http\Controllers\API;


use App\table_api_key;
use App\table_btmk;
use App\table_lelang_sk;
use App\table_log;
use App\table_suratkuasa;
use App\table_user;
use Illuminate\Http\Request;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use Illuminate\Support\Facades\Auth;
use Validator;
use Hash;



class getLelangSK extends APIBaseController
{
    public function log(Request $request)
    {
        $date = date('Y-m-d H:m:s');
        if (table_api_key::where('api_key', '=', $request->get('apiKey'))->exists()) {
            $validator = Validator::make($request->all(), [
                'id_user' => 'required',

            ]);
            if ($validator->fails()) {
                $id = $request->get('id_user');
                table_log::insert(array(
                    'ws_name' => 'getLelangSK.php',
                    'message_log' => 'Data Tidak Sesuai',
                    'created_at' => $date,
                    'id_user' => $id,
                ));
                return response()->json(['error' => $validator->errors()], 401);
            }
            else {
                $id = $request->get('id_user');
                $data = table_lelang_sk::join('table_detail_lelang_sk','table_lelang_sk.id_lsk','=',
                    'table_detail_lelang_sk.id_lsk')
                    ->select('table_lelang_sk.id_lsk','table_lelang_sk.nama_lsk','table_lelang_sk.photo_lsk',
                        'table_lelang_sk.no_aggr_lsk','table_lelang_sk.jumlah_bto_lsk','table_lelang_sk.lowest_bto_lsk'
                        ,'table_lelang_sk.deskripsi_lsk','table_lelang_sk.start_date_lsk','table_lelang_sk.end_date_lsk',
                        'table_lelang_sk.nama_lsk')
                    ->where('table_detail_lelang_sk.id_reg_mitra_dlsk','=',$id)
                    ->whereRaw('DATE(end_date_lsk) >= CURRENT_DATE')
                    ->whereRaw('DATE(start_date_lsk) <= CURRENT_DATE')
                    ->where('table_detail_lelang_sk.lowest_bto_dlsk','=',null)
                    ->where('table_lelang_sk.is_deleted','=',0)
                    ->get();
                if(isset($data)){
                    table_log::insert(array(
                        'ws_name' => 'getLelangSK.php',
                        'message_log' => 'Data Sesuai',
                        'created_at' => $date,
                        'id_user' => $id,
                    ));
                    return $this->sendResponse7($data, 'Log Berhasil Di-input / Data Sesuai');
                }
                else{
                    table_log::insert(array(
                        'ws_name' => 'getLelangSK.php',
                        'message_log' => 'Data Tidak Sesuai',
                        'created_at' => $date,
                        'id_user' => $id,
                    ));
                    return $this->sendError('Log Berhasil Di-input / Data Tidak Sesuai');
                }
            }
        }
        else{
            table_log::insert(array(
                'ws_name' => 'getLelangSK.php',
                'message_log' => 'Cek gagal',
                'created_at' => $date,
                'id_user' => '0',
            ));
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }
}