<?php


namespace App\Http\Controllers\API;


use App\table_api_key;
use App\table_log;
use App\table_user;
use Illuminate\Http\Request;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use Validator;
use Hash;



class doPerbaruiKataSandi extends APIBaseController
{
    public function Store(Request $request)
    {
        $date = date('Y-m-d H:m:s');
        if (table_api_key::where('api_key', '=', $request->get('apiKey'))->exists()) {
            $input = $request->all();
            $validator = Validator::make($input, [
                'password' => 'required',
                'id_user' => 'required'
            ]);
            $id_user = $request->get('id_user');
            if ($validator->fails()) {
                table_log::insert(array(
                    'ws_name' => 'doPerbaruiKataSandi.php',
                    'message_log' => 'Password gagal diganti',
                    'created_at' => $date,
                    'id_user' => $id_user,
                ));
                return $this->sendError('Validation Error.', $validator->errors());
            }
            else {
                $post = table_user::find($id_user);
                if($post->is_deleted == 2){
                    $post->is_deleted = 0;
                    $post->password = bcrypt($input['password']);
                    $post->save();
                    table_log::insert(array(
                        'ws_name' => 'doPerbaruiKataSandi.php',
                        'message_log' => 'Password berhasil diganti',
                        'created_at' => $date,
                        'id_user' => $id_user,
                    ));
                    if ($post->save()) {
                        return $this->sendResponse1('Kata Sandi Berhasil Diperbarui');
                    } else {
                        return $this->sendError('Kata Sandi Gagal Diperbarui');
                    }

                }
                else {
                    $post->password = bcrypt($input['password']);
                    $post->save();
                    table_log::insert(array(
                        'ws_name' => 'doPerbaruiKataSandi.php',
                        'message_log' => 'Password berhasil diganti',
                        'created_at' => $date,
                        'id_user' => $id_user,
                    ));
                    if ($post->save()) {
                        return $this->sendResponse1('Kata Sandi Berhasil Diperbarui');
                    } else {
                        return $this->sendError('Kata Sandi Gagal Diperbarui');
                    }
                }
            }
        }
        else{
            table_log::insert(array(
                'ws_name' => 'doPerbaruiKataSandi.php',
                'message_log' => 'Password gagal diganti',
                'created_at' => $date,
                'id_user' => '0',
            ));
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }


}