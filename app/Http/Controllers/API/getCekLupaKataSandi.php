<?php


namespace App\Http\Controllers\API;


use App\Mail\OTPPassword;
use App\table_api_key;
use App\Mail\SendEmail;
use App\table_log;
use App\table_user;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use Validator;
use Hash;



class getCekLupaKataSandi extends APIBaseController
{
    public function index(Request $request)
    {
        $date = date('Y-m-d H:m:s');
        if (table_api_key::where('api_key', '=', $request->get('apiKey'))->exists()) {
            $validator = Validator::make($request->all(), [
                'cek_lupakatasandi' => 'required'

            ]);
            if ($validator->fails()) {
                table_log::insert(array(
                    'ws_name' => 'getCekLupaKataSandi.php',
                    'message_log' => 'Cek gagal',
                    'created_at' => $date,
                    'id_user' => '0',
                ));
                return response()->json(['error' => $validator->errors()], 401);
            }
            else {
                $data = $request->get('cek_lupakatasandi');
                $post = table_user::where('is_deleted', '!=', 1)
                    ->where('email', '=', $data)
                    ->orWhere('no_hp', '=', $data)
                    ->first();
                if (is_null($post)) {
                    table_log::insert(array(
                        'ws_name' => 'getCekLupaKataSandi.php',
                        'message_log' => 'Cek berhasil',
                        'created_at' => $date,
                        'id_user' => '0',
                    ));
                    return $this->sendError('Data tidak ditemukan');
                }
                else {
                    $success['id_user'] = $post->id_user;
                    do{
                        $str =  str_random(4);
                    }while(table_user::where('otp','=',$str)->exists());
                    table_user::where('email','=',$post->email)
                        ->orwhere('no_hp','=',$post->no_hp)
                        ->update(array(
                            'otp' => strtoupper($str)
                        ));
                    $user = User::where('is_deleted', '!=', 1)
                        ->where('email', '=', $data)
                        ->orWhere('no_hp', '=', $data)
                        ->first();
                    \Mail::to($user)->send(new OTPPassword($user));
                    table_log::insert(array(
                        'ws_name' => 'getCekLupaKataSandi.php',
                        'message_log' => 'Cek berhasil',
                        'created_at' => $date,
                        'id_user' => $post->id_user,
                    ));
                    return $this->sendResponse($success, 'Data sesuai');
                }
            }
        }
        else{
            table_log::insert(array(
                'ws_name' => 'getCekLupaKataSandi.php',
                'message_log' => 'Cek gagal',
                'created_at' => $date,
                'id_user' => '0',
            ));
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }
}