<?php


namespace App\Http\Controllers\API;


use App\table_api_key;
use App\table_laporan;
use App\table_log;
use App\table_suratkuasa;
use App\table_user;
use Illuminate\Http\Request;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use Validator;
use Hash;



class doMintaSK extends APIBaseController
{
    public function Store(Request $request)
    {
        $date = date('Y-m-d H:m:s');
        if (table_api_key::where('api_key', '=', $request->get('apiKey'))->exists()) {
            $input = $request->all();
            $validator = Validator::make($input, [
                'id_user' => 'required',
                'bto_request' => 'required',
                'id_btmk' => 'required',
                'desc_sp' => 'required',
                'area' => 'required',
            ]);
            $id_user = $request->get('id_user');
            if ($validator->fails()) {
                table_log::insert(array(
                    'ws_name' => 'doMintaSK.php',
                    'message_log' => 'Data Gagal Disimpan   ',
                    'created_at' => $date,
                    'id_user' => $id_user,
                ));
                return $this->sendError('Validation Error.', $validator->errors());
            }
            else {
                $post = new table_suratkuasa();
                $post->id_user = $input['id_user'];
                $post->bto_request = $input['bto_request'];
                $post->id_btmk = $input['id_btmk'];
                $post->desc_sp = $input['desc_sp'];
                $post->area = $input['area'];
                $post->created_at = $date;
                $post->created_by = $id_user;
                $post->save();

                if ($post->save()){
                    $id_lapor = table_laporan::where('id_btmk','=', $post->id_btmk)
                        ->first();
                    $data['id_lapor'] = $id_lapor->id_lapor;
                    $data['id_btmk'] = $post->id_btmk;
                    $data['desc_sp'] = $post->desc_sp;
                    $data['area'] = $post->area;

                    table_log::insert(array(
                        'ws_name' => 'doMintaSK.php',
                        'message_log' => 'Data Gagal Disimpan',
                        'created_at' => $date,
                        'id_user' => $id_user,
                    ));

                    return $this->sendResponse3($data,'Log Berhasil Di-input / Data Berhasil Disimpan');
                }
                else{
                    table_log::insert(array(
                        'ws_name' => 'doMintaSK.php',
                        'message_log' => 'Data Gagal Disimpan',
                        'created_at' => $date,
                        'id_user' => $id_user,
                    ));

                    return $this->sendError('Log Berhasil Di-input / Data Gagal Disimpan');
                }

            }
        }
        else{
            table_log::insert(array(
                'ws_name' => 'doMintaSK.php',
                'message_log' => 'Data Gagal Disimpan',
                'created_at' => $date,
                'id_user' => '0',
            ));
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }


}