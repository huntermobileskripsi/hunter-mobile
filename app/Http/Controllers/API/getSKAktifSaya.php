<?php


namespace App\Http\Controllers\API;


use App\table_api_key;
use App\table_btmk;
use App\table_log;
use App\table_suratkuasa;
use App\table_user;
use Illuminate\Http\Request;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use Illuminate\Support\Facades\Auth;
use Validator;
use Hash;



class getSKAktifSaya extends APIBaseController
{
    public function log(Request $request)
    {
        $date = date('Y-m-d H:m:s');
        if (table_api_key::where('api_key', '=', $request->get('apiKey'))->exists()) {
            $validator = Validator::make($request->all(), [
                'id_user' => 'required',

            ]);
            if ($validator->fails()) {
                $id = $request->get('id_user');
                table_log::insert(array(
                    'ws_name' => 'getSKAktifSaya.php',
                    'message_log' => 'Data Tidak Sesuai',
                    'created_at' => $date,
                    'id_user' => $id,
                ));
                return response()->json(['error' => $validator->errors()], 401);
            }
            else {
                $id = $request->get('id_user');
                $dat = table_suratkuasa::join('table_btmk','table_suratkuasa.id_btmk','=','table_btmk.id_btmk')
                    ->join('table_user','table_suratkuasa.approved_by','=','table_user.id_user')
                    ->leftjoin('table_user as t','table_suratkuasa.perpanjangan_approved_by','=','t.id_user')
                    ->select('table_suratkuasa.id_suratkuasa','table_suratkuasa.id_btmk','table_btmk.NO_CAR_POLICE',
                        'table_suratkuasa.desc_sp','table_suratkuasa.area','table_suratkuasa.approved_by','table_suratkuasa.approved_at',
                        'table_suratkuasa.end_approved_at','table_suratkuasa.perpanjangan_approved_at','table_suratkuasa.perpanjangan_approved_by',
                        'table_suratkuasa.end_perpanjangan_approved_at','table_suratkuasa.updated_at','table_suratkuasa.created_at',
                        'table_suratkuasa.flag_perpanjangan','table_suratkuasa.flag_approved','table_user.nama_lengkap',
                        't.nama_lengkap as nama','table_suratkuasa.id_user','table_suratkuasa.updated_at_p')
                    ->Where(function($query) use ($date)
                    {
                        $query->where('table_suratkuasa.approved_by','!=',null)
                            ->orwhere('table_suratkuasa.end_approved_at','>=', $date)
                            ->orwhere('table_suratkuasa.perpanjangan_approved_by','!=',null)
                            ->orwhere('table_suratkuasa.end_perpanjangan_approved_at','>=', $date);
                    })
                    ->where('table_suratkuasa.flag_approved','=','Y')
		    ->where('table_suratkuasa.id_user','=',$id)
                    ->where('table_suratkuasa.is_deleted','=',0)
                    ->get();
                if(isset($dat)&& count($dat) > 0){
                    table_log::insert(array(
                        'ws_name' => 'getSKAktifSaya.php',
                        'message_log' => 'Data Sesuai',
                        'created_at' => $date,
                        'id_user' => $id,
                    ));
                    foreach ($dat as $data) {
                        $post[] = [

                            'id_suratkuasa' => $data->id_suratkuasa,
                            'id_btmk' => $data->id_btmk,
                            'no_car_police' => $data->NO_CAR_POLICE,
                            'desc_sp' => $data->desc_sp,
                            'area' => $data->area,
                            'approved_by' => $data->nama_lengkap,
                            'approved_at' => $data->approved_at,
                            'end_approved_at' => $data->end_approved_at,
                            'flag_approved' => $data->flag_approved,
                            'perpanjangan_approved_at' => $data->perpanjangan_approved_at,
                            'perpanjangan_approved_by' => $data->nama,
                            'end_perpanjangan_approved_at' => $data->end_perpanjangan_approved_at,
                            'flag_perpanjangan' => $data->flag_perpanjangan,
                            'updated_at_p' => $data->updated_at_p,
                            'created_at' => $data->created_at,

                        ];
                    }
                    return $this->sendResponse6($post, 'Log Berhasil Di-input / Data Sesuai');
                }
                elseif (isset($dat)&& count($dat) == 0){
                    table_log::insert(array(
                        'ws_name' => 'getSKAktifSaya.php',
                        'message_log' => 'Data Sesuai',
                        'created_at' => $date,
                        'id_user' => $id,
                    ));
                    return $this->sendResponse6($dat, 'Log Berhasil Di-input / Data Sesuai');
                }
                else{
                    table_log::insert(array(
                        'ws_name' => 'getSKAktifSaya.php',
                        'message_log' => 'Data Tidak Sesuai',
                        'created_at' => $date,
                        'id_user' => $id,
                    ));
                    return $this->sendError('Log Berhasil Di-input / Data Tidak Sesuai');
                }
            }
        }
        else{
            table_log::insert(array(
                'ws_name' => 'getSKAktifSaya.php',
                'message_log' => 'Cek gagal',
                'created_at' => $date,
                'id_user' => '0',
            ));
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }
}