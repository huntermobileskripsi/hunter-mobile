<?php


namespace App\Http\Controllers\API;


use App\table_api_key;
use App\table_laporan;
use App\table_log;
use App\table_user;
use Illuminate\Http\Request;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use Validator;
use Hash;



class doPerbaruiDraftSaya extends APIBaseController
{
    public function Store(Request $request)
    {
        $date = date('Y-m-d H:m:s');
        if (table_api_key::where('api_key', '=', $request->get('apiKey'))->exists()) {
            $input = $request->all();
            $validator = Validator::make($input, [
                'id_user' => 'required',
                'id_lapor' => 'required',
                'pict_thumbnail' => 'required',
                'pict_thumbnail2' => 'required',
                'description' => 'required',
            ]);
            $id_user = $request->get('id_user');
            if ($validator->fails()) {
                table_log::insert(array(
                    'ws_name' => 'doPerbaruiDraftSaya.php',
                    'message_log' => 'Data Gagal Disimpan',
                    'created_at' => $date,
                    'id_user' => $id_user,
                ));
                return $this->sendError('Validation Error.', $validator->errors());
            }
            else {
                $idlapor = $request->get('id_lapor');
                $post = table_laporan::find($idlapor);
                $file = $request->file('pict_thumbnail');
                $file2 = $request->file('pict_thumbnail2');
                $post->description = $input['description'];

                $fileName = $file->getClientOriginalName();
                $destinationPath = public_path().'/images/';
                $file->move($destinationPath,$fileName);
                $post->pict_tumbnail = $fileName;

                $fileName2 = $file2->getClientOriginalName();
                $destinationPath2 = public_path().'/images/';
                $file2->move($destinationPath2,$fileName2);
                $post->pict_tumbnail2 = $fileName2;

                $post->save();
                if($post->save()) {
                    table_log::insert(array(
                        'ws_name' => 'doPerbaruiDraftSaya.php',
                        'message_log' => 'Data Berhasil Disimpan',
                        'created_at' => $date,
                        'id_user' => $id_user,
                    ));
                    return $this->sendResponse1('Log Berhasil Di-input / Data Berhasil Disimpan');
                }
                else{
                    table_log::insert(array(
                        'ws_name' => 'doPerbaruiDraftSaya.php',
                        'message_log' => 'Data Gagal Disimpan',
                        'created_at' => $date,
                        'id_user' => $id_user,
                    ));
                    return $this->sendError('Log Berhasil Di-input / Data Gagal Disimpan');
                }
            }
        }
        else{
            table_log::insert(array(
                'ws_name' => 'doPerbaruiDraftSaya.php',
                'message_log' => 'Data Gagal Disimpan',
                'created_at' => $date,
                'id_user' => '0',
            ));
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }


}