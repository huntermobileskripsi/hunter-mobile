<?php


namespace App\Http\Controllers\API;


use App\table_api_key;
use App\table_log;
use App\table_user;
use Illuminate\Http\Request;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use Validator;
use Hash;



class doUpdateData extends APIBaseController
{
    public function Store(Request $request)
    {
        $date = date('Y-m-d H:m:s');
        if (table_api_key::where('api_key', '=', $request->get('apiKey'))->exists()) {
            $input = $request->all();
            $validator = Validator::make($input, [
                'id_user' => '',
                'nama_lengkap' => '',
                'email' => '',
                'alamat' => '',
                'no_hp' => '',
                'no_ktp' => '',
            ]);
            $id_user = $request->get('id_user');
            if ($validator->fails()) {
                table_log::insert(array(
                    'ws_name' => 'doUpdateData.php',
                    'message_log' => 'Update Data User Gagal',
                    'created_at' => $date,
                    'id_user' => $id_user,
                ));
                return $this->sendError('Validation Error.', $validator->errors());
            }
            else {
                $post = table_user::find($id_user);
                if(table_user::where('id_user','!=', $id_user)
                    ->where('is_deleted','!=','1')
                    ->where('email','=',$request->get('email'))
                    ->exists())
                {
                    table_log::insert(array(
                        'ws_name' => 'doUpdateData.php',
                        'message_log' => 'Update Data User Gagal',
                        'created_at' => $date,
                        'id_user' => $id_user,
                    ));
                    return $this->sendError('Update Data User Gagal');

                }
                else{
                    if (table_user::where('id_user','!=',$id_user)
                        ->where('is_deleted','!=','1')
                        ->where('no_hp','=', $request->get('no_hp'))
                        ->exists())
                    {
                        table_log::insert(array(
                            'ws_name' => 'doUpdateData.php',
                            'message_log' => 'Update Data User Gagal',
                            'created_at' => $date,
                            'id_user' => $id_user,
                        ));
                        return $this->sendError('Update Data User Gagal');
                    }
                    else{
                        $post->nama_lengkap = strtoupper($request->get('nama_lengkap'));
                        $post->email = $request->get('email');
                        $post->alamat = $request->get('alamat');
                        $post->no_hp = $request->get('no_hp');
                        $post->no_ktp = $request->get('no_ktp');
                        $post->updated_at = $date;
                        $post->save();

                        if($post->save()) {
                            table_log::insert(array(
                                'ws_name' => 'doUpdateData.php',
                                'message_log' => 'Update Data User Berhasil',
                                'created_at' => $date,
                                'id_user' => $id_user,
                            ));
                            return $this->sendResponse1('Update Data User Berhasil');
                        }
                        else{
                            table_log::insert(array(
                                'ws_name' => 'doUpdateData.php',
                                'message_log' => 'Update Data User Gagal',
                                'created_at' => $date,
                                'id_user' => $id_user,
                            ));
                            return $this->sendError('Update Data User Gagal');
                        }
                    }
                }

            }
        }
        else{
            table_log::insert(array(
                'ws_name' => 'doUpdateData.php',
                'message_log' => 'Update Data User Gagal',
                'created_at' => $date,
                'id_user' => '0',
            ));
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }


}