<?php


namespace App\Http\Controllers\API;


use App\table_api_key;
use App\table_btmk;
use App\table_log;
use App\table_user;
use Illuminate\Http\Request;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use Validator;
use Hash;



    class cariKendaraan extends APIBaseController
{
    public function Index(Request $request)
    {
        $date = date('Y-m-d H:m:s');
        if (table_api_key::where('api_key', '=', $request->get('apiKey'))->exists()) {
            $input = $request->all();
            $validator = Validator::make($input, [
                'cek_nomor' => 'required'
            ]);
            $id_user = $request->get('id_user');
            if ($validator->fails()) {
                table_log::insert(array(
                    'ws_name' => 'cariKendaraan.php',
                    'message_log' => 'Cari kendaraan gagal',
                    'created_at' => $date,
                    'id_user' => $id_user,
                ));
                return $this->sendError('Validation Error.', $validator->errors());
            }
            else {
                $data = $request->get('cek_nomor');
                $dat = table_btmk::select('id_btmk','NO_CAR_POLICE','DESC_VEHICLE_BRAND','DESC_VEHICLE_TYPE',
                    'DESC_VEHICLE_MODEL','COLOR','NO_ENGINE','DESC_SP','AREA2','NO_CHASIS')
                    ->where('NO_CAR_POLICE', '=', $data)
                    ->orwhere('NO_ENGINE', '=', $data)
                    ->orwhere('NO_CHASIS', '=', $data)
                    ->first();
                if(isset($dat) && count($dat) > 0) {
                    $post['id_btmk'] = $dat->id_btmk;
                    $post['no_car_police'] = $dat->NO_CAR_POLICE;
                    $post['desc_vehicle_brand'] = $dat->DESC_VEHICLE_BRAND;
                    $post['desc_vehicle_type'] = $dat->DESC_VEHICLE_TYPE;
                    $post['desc_vehicle_model'] = $dat->DESC_VEHICLE_MODEL;
                    $post['color'] = $dat->COLOR;
                    $post['no_chasis'] = $dat->NO_CHASIS;
                    $post['no_engine'] = $dat->NO_ENGINE;
                    $post['desc_sp'] = $dat->DESC_SP;
                    $post['area'] = $dat->AREA2;
                    table_log::insert(array(
                        'ws_name' => 'cariKendaraan.php',
                        'message_log' => 'Cari kendaraan berhasil',
                        'created_at' => $date,
                        'id_user' => $id_user,
                    ));
                    return $this->sendResponse4($post, 'Log Berhasil Di-input / Data Sesuai');
                }
                else{
                    table_log::insert(array(
                        'ws_name' => 'cariKendaraan.php',
                        'message_log' => 'Cari kendaraan gagal',
                        'created_at' => $date,
                        'id_user' => $id_user,
                    ));
                    return $this->sendError('Log Berhasil Di-input / Data Tidak Sesuai');
                }
            }
        }
        else{
            table_log::insert(array(
                'ws_name' => 'cariKendaraan.php',
                'message_log' => 'Cari kendaraan gagal',
                'created_at' => $date,
                'id_user' => '0',
            ));
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }


}