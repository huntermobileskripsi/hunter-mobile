<?php


namespace App\Http\Controllers\API;


use App\table_api_key;
use App\table_laporan;
use App\table_log;
use App\table_suratkuasa;
use App\table_user;
use Illuminate\Http\Request;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use Illuminate\Support\Facades\Auth;
use Validator;
use Hash;



class getDraftSaya extends APIBaseController
{
    public function log(Request $request)
    {
        $date = date('Y-m-d H:m:s');
        if (table_api_key::where('api_key', '=', $request->get('apiKey'))->exists()) {
            $validator = Validator::make($request->all(), [
                'id_user' => 'required',
                'flag_draft' => 'required'

            ]);
            if ($validator->fails()) {
                $id = $request->get('id_user');
                table_log::insert(array(
                    'ws_name' => 'getDraftSaya.php',
                    'message_log' => 'Data Gagal Disimpan',
                    'created_at' => $date,
                    'id_user' => $id,
                ));
                return response()->json(['error' => $validator->errors()], 401);
            }
            else {
                $id = $request->get('id_user');
                $flag = $request->get('flag_draft');
                $dat = table_laporan::join('table_btmk','table_laporan.id_btmk','=','table_btmk.id_btmk')
                    ->select('table_laporan.id_lapor','table_laporan.id_btmk','table_btmk.NO_CAR_POLICE','table_btmk.DESC_VEHICLE_BRAND',
                        'table_btmk.DESC_VEHICLE_TYPE','table_btmk.DESC_VEHICLE_MODEL','table_btmk.COLOR','table_btmk.NO_ENGINE',
                        'table_btmk.DESC_SP','table_laporan.pict_tumbnail','table_laporan.pict_tumbnail2','table_laporan.lat_lapor',
                        'table_laporan.long_lapor','table_laporan.description','table_btmk.NO_CHASIS','table_btmk.AREA2')
                    ->where('table_laporan.id_user','=',$id)
                    ->where('table_laporan.flag_draft','=',$flag)
                    ->where('table_laporan.is_deleted','=',0)
                    ->get();
                if(isset($dat)&& count($dat) > 0){
                    foreach ($dat as $data) {
                        $post[] = [

                            'id_lapor' => $data->id_lapor,
                            'id_btmk' => $data->id_btmk,
                            'no_car_police' => $data->NO_CAR_POLICE,
                            'desc_vehicle_brand' => $data->DESC_VEHICLE_BRAND,
                            'desc_vehicle_type' => $data->DESC_VEHICLE_TYPE,
                            'desc_vehicle_model' => $data->DESC_VEHICLE_MODEL,
                            'color' => $data->COLOR,
                            'no_engine' => $data->NO_ENGINE,
                            'desc_sp' => $data->DESC_SP,
                            'no_chasis' => $data->NO_CHASIS,
                            'pict_thumbnail' => $data->pict_tumbnail,
                            'pict_thumbnail2' => $data->pict_tumbnail2,
                            'lat_lapor' => $data->lat_lapor,
                            'long_lapor' => $data->long_lapor,
                            'description' => $data->description,
                            'area' => $data->AREA2

                        ];
                    }

                    table_log::insert(array(
                        'ws_name' => 'getDraftSaya.php',
                        'message_log' => 'Data Berhasil Disimpan',
                        'created_at' => $date,
                        'id_user' => $id,
                    ));
                    return $this->sendResponse5($post,'Log Berhasil Di-input / Data Berhasil Disimpan');
                }
                elseif (isset($dat)&& count($dat) == 0){
                    table_log::insert(array(
                        'ws_name' => 'getDraftSaya.php',
                        'message_log' => 'Data Berhasil Disimpan',
                        'created_at' => $date,
                        'id_user' => $id,
                    ));
                    return $this->sendResponse5($dat, 'Log Berhasil Di-input / Data Berhasil Disimpan');
                }
                else{
                    table_log::insert(array(
                        'ws_name' => 'getDraftSaya.php',
                        'message_log' => 'Data Gagal Disimpan',
                        'created_at' => $date,
                        'id_user' => $id,
                    ));
                    return $this->sendError('Log Berhasil Di-input / Data Gagal Disimpan');
                }
            }
        }
        else{
            table_log::insert(array(
                'ws_name' => 'getDraftSaya.php',
                'message_log' => 'Cek gagal',
                'created_at' => $date,
                'id_user' => '0',
            ));
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }
}