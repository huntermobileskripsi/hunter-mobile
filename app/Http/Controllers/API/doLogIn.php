<?php

namespace App\Http\Controllers\API;

use App\Mail\SendEmail;
use App\OauthAccessToken;
use App\table_api_key;
use App\table_log;
use App\table_suratkuasa;
use App\table_user;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\DB;
use carbon\carbon;

class doLogIn extends APIBaseController
{
    public $successStatus = 200;
    /**
     * login api
     *a
     * @return \Illuminate\Http\Response
     */

    public function login(Request $request){
        $date = date('Y-m-d H:m:s');
        if(table_api_key::where('api_key','=', $request->get('apiKey'))->exists()) {
            if (Auth::attempt(['email' => request('email'), 'password' => request('password'), 'is_deleted' => 0])) {
                $user = Auth::user();
                $id = Auth::id();
//                if (OauthAccessToken::where('user_id','=',$user->id_user)
//                        ->where('revoked','=','0')
//                        ->count() >= 1){
//                    return $this->sendError('Silahkan Log out terlebih dahulu');
//                }
//                else {
                if (table_user::where('id_user','=',$user->id_user)
                    ->where('is_deleted','!=','0')
                    ->first()){
                    return $this->sendError('Akun tidak terdaftar');
                }
                else {
                    if (OauthAccessToken::where('user_id','=',$user->id_user)
                            ->where('revoked','=','0')
                            ->count() >= 1){
                        $del = OauthAccessToken::where('user_id','=',$user->id_user)->first();
                        $del->revoked = 1;
                        $del->save();

                        $user->date_login = date('Y-m-d H:m:s');
                        $user->save();
                        $date = date('Y-m-d H:m:s');
                        $sk = table_suratkuasa::where('id_user', '=', $user->id_user)
                            ->where('is_deleted', '=', '0')
                            ->count();
                        $success['id_user'] = $user->id_user;
                        $success['jumlah_sk_aktif'] = $sk;
                        $success['nama_lengkap'] = $user->nama_lengkap;
                        $success['alamat'] = $user->alamat;
                        $success['no_ktp'] = $user->no_ktp;
                        $success['no_hp'] = $user->no_hp;
                        $success['email'] = $user->email;
                        $success['role'] = $user->role;
                        $success['no_hp'] = $user->no_hp;
                        $success['token'] = $user->createToken('Hunter')->accessToken;
                        table_log::insert(array(
                            'ws_name' => 'doLogIn.php',
                            'message_log' => 'Login berhasil',
                            'created_at' => $date,
                            'id_user' => $id,
                        ));
                        return $this->sendResponse($success, 'Login Berhasil');
                    }
                    else {
                        $user->date_login = date('Y-m-d H:m:s');
                        $user->save();
                        $date = date('Y-m-d H:m:s');
                        $sk = table_suratkuasa::where('id_user', '=', $user->id_user)
                            ->where('is_deleted', '=', '0')
                            ->count();
                        $success['id_user'] = $user->id_user;
                        $success['jumlah_sk_aktif'] = $sk;
                        $success['nama_lengkap'] = $user->nama_lengkap;
                        $success['alamat'] = $user->alamat;
                        $success['no_ktp'] = $user->no_ktp;
                        $success['no_hp'] = $user->no_hp;
                        $success['email'] = $user->email;
                        $success['role'] = $user->role;
                        $success['no_hp'] = $user->no_hp;
                        $success['token'] = $user->createToken('Hunter')->accessToken;
                        table_log::insert(array(
                            'ws_name' => 'doLogIn.php',
                            'message_log' => 'Login berhasil',
                            'created_at' => $date,
                            'id_user' => $id,
                        ));
                        return $this->sendResponse($success, 'Login Berhasil');
                    }
                }
//                }
            }
            else{
                table_log::insert(array(
                    'ws_name' => 'doLogIn.php',
                    'message_log' => 'Login gagal',
                    'created_at' => $date,
                    'id_user' => '0',
                ));
                return $this->sendError('Login gagal');
            }
        }
        else{
            table_log::insert(array(
                'ws_name' => 'doLogIn.php',
                'message_log' => 'Login gagal',
                'created_at' => $date,
                'id_user' => '0',
            ));
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }
}
