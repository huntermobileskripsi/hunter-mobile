<?php


namespace App\Http\Controllers\API;


use App\table_api_key;
use App\table_laporan;
use App\table_log;
use App\table_user;
use Illuminate\Http\Request;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use Illuminate\Support\Facades\Auth;
use Validator;
use Hash;



class getRiwayat extends APIBaseController
{
    public function log(Request $request)
    {

        $date = date('Y-m-d H:m:s');
        if (table_api_key::where('api_key', '=', $request->get('apiKey'))->exists()){
            $id_user = $request->get('id_user');
            $month = date('m');
            if($request->get('role') == 1){
                $data = table_log::select('id_log','message_log','created_at')
                    ->Where(function($query)
                    {
                        $query->where('ws_name','=','lapor.php/doLapor')
                            ->orwhere('ws_name','=','lapor.php/doSaveToDraft');
                    })
                    ->whereRaw('MONTH(created_at) = ?', [$month])
                    ->where('id_user','=',$id_user)
                    ->whereRaw('DATE(created_at) = CURRENT_DATE')
                    ->get();
                table_log::insert(array(
                    'ws_name' => 'getRiwayat.php',
                    'message_log' => 'Log Riwayat Attempt',
                    'created_at' => $date,
                    'id_user' => $id_user,
                ));
                table_log::insert(array(
                    'ws_name' => 'getRiwayat.php',
                    'message_log' => 'Get riwayat berhasil',
                    'created_at' => $date,
                    'id_user' => $id_user,
                ));
                return $this->sendResponse2($data,'Log berhasil Di-input/get riwayat berhasil');
            }
            else{
                $data = table_log::select('id_log','message_log','created_at')
                    ->Where(function($query)
                    {
                        $query->where('ws_name','=','lapor.php/doLapor')
                            ->orwhere('ws_name','=','lapor.php/doSaveToDraft')
                            ->orwhere('ws_name','=','lelangsk.php/doBidLelangSK');
                    })
                    ->whereRaw('MONTH(created_at) = ?', [$month])
                    ->where('id_user','=',$id_user)
                    ->whereRaw('DATE(created_at) = CURRENT_DATE')
                    ->get();
                table_log::insert(array(
                    'ws_name' => 'getRiwayat.php',
                    'message_log' => 'Log Riwayat Attempt',
                    'created_at' => $date,
                    'id_user' => $id_user,
                ));
                table_log::insert(array(
                    'ws_name' => 'getRiwayat.php',
                    'message_log' => 'Get riwayat berhasil',
                    'created_at' => $date,
                    'id_user' => $id_user,
                ));
                return $this->sendResponse2($data,'Log berhasil Di-input/get riwayat berhasil');
            }
        }
        else{
            table_log::insert(array(
                'ws_name' => 'getRiwayat.php',
                'message_log' => 'Cek gagal',
                'created_at' => $date,
                'id_user' => '0',
            ));
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }
}