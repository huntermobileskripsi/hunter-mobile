<?php

namespace App\Http\Controllers\API;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;


class APIBaseController extends Controller
{


    public function sendResponse($result, $message)
    {
        $response = [
            'userData'=> $result,
            'status' => true,
            'message' => $message,
        ];


        return response()->json($response, 200);
    }

    public function sendResponse1($result)
    {
        $response = [
            'status' => true,
            'message' => $result,
        ];


        return response()->json($response, 200);
    }

    public function sendResponse2($result, $message)
    {
        $response = [
            'logData'=> $result,
            'status' => true,
            'message' => $message,
        ];


        return response()->json($response, 200);
    }

    public function sendResponse3($result, $message)
    {
        $response = [
            'laporData'=> $result,
            'status' => true,
            'message' => $message,
        ];


        return response()->json($response, 200);
    }

    public function sendResponse4($result, $message)
    {
        $response = [
            'unitData'=> $result,
            'status' => true,
            'message' => $message,
        ];


        return response()->json($response, 200);
    }

    public function sendResponse5($result, $message)
    {
        $response = [
            'draftData'=> $result,
            'status' => true,
            'message' => $message,
        ];


        return response()->json($response, 200);
    }

    public function sendResponse6($result, $message)
    {
        $response = [
            'skData'=> $result,
            'status' => true,
            'message' => $message,
        ];


        return response()->json($response, 200);
    }

    public function sendResponse7($result, $message)
    {
        $response = [
            'lskData'=> $result,
            'status' => true,
            'message' => $message,
        ];


        return response()->json($response, 200);
    }


    public function sendError($error, $errorMessages = [], $code = 404)
    {
        $response = [
            'status' => false,
            'message' => $error,
        ];


        if(!empty($errorMessages)){
            $response['data'] = $errorMessages;
        }


        return response()->json($response, $code);
    }
}