<?php


namespace App\Http\Controllers\API;


use App\table_api_key;
use App\table_btmk;
use App\table_detail_lelang_sk;
use App\table_lelang_sk;
use App\table_log;
use App\table_suratkuasa;
use App\table_user;
use Illuminate\Http\Request;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use Illuminate\Support\Facades\Auth;
use Validator;
use Hash;



class getPemenangLSK extends APIBaseController
{
    public function log(Request $request)
    {
        $date = date('Y-m-d H:m:s');
        if (table_api_key::where('api_key', '=', $request->get('apiKey'))->exists()) {
            $validator = Validator::make($request->all(), [
                'id_user' => 'required',

            ]);
            if ($validator->fails()) {
                $id = $request->get('id_user');
                table_log::insert(array(
                    'ws_name' => 'getPemenangLSK.php',
                    'message_log' => 'Data Tidak Sesuai',
                    'created_at' => $date,
                    'id_user' => $id,
                ));
                return response()->json(['error' => $validator->errors()], 401);
            }
            else {
                $month = date('m');
                $id = $request->get('id_user');
                $data = table_detail_lelang_sk::join('table_lelang_sk','table_detail_lelang_sk.id_lsk','=',
                    'table_lelang_sk.id_lsk')
                    ->select('table_lelang_sk.id_lsk','table_lelang_sk.nama_lsk','table_lelang_sk.photo_lsk',
                        'table_lelang_sk.no_aggr_lsk','table_lelang_sk.jumlah_bto_lsk','table_lelang_sk.lowest_bto_lsk'
                        ,'table_lelang_sk.deskripsi_lsk','table_lelang_sk.start_date_lsk','table_lelang_sk.end_date_lsk',
                        'table_detail_lelang_sk.flag_pemenang_dlsk')
                    ->Where(function($query) use ($id,$month)
                    {
                        $query->where('table_detail_lelang_sk.id_reg_mitra_dlsk','=', $id)
                            ->whereRaw('MONTH(table_lelang_sk.end_date_lsk) = ?', [$month])
                            ->where('table_detail_lelang_sk.lowest_bto_dlsk','!=',null)
                            ->where('table_detail_lelang_sk.is_deleted','=','1');
                    })
                    ->orWhere(function($query) use ($id,$month)
                    {
                        $query->where('table_detail_lelang_sk.id_reg_mitra_dlsk','=', $id)
                            ->whereRaw('MONTH(table_lelang_sk.end_date_lsk) = ?', [$month])
                            ->where('table_detail_lelang_sk.lowest_bto_dlsk','!=',null)
                            ->where('table_detail_lelang_sk.is_deleted','=','0');
                    })
                    ->orderBy('table_lelang_sk.updated_at','DESC')
                    ->get();
//                $data2 = table_detail_lelang_sk::join('table_lelang_sk','table_detail_lelang_sk.id_lsk','=',
//                    'table_lelang_sk.id_lsk')
//                    ->select('table_detail_lelang_sk.id_reg_mitra_dlsk','table_detail_lelang_sk.id_lsk')
//                    ->where('table_detail_lelang_sk.flag_pemenang_lsk','=','Y')
////                    ->whereRaw('MONTH(table_lelang_sk.end_date_lsk) = ?', [$month])
//                    ->where('table_detail_lelang_sk.is_deleted','=',0)
//                    ->get();
                $data2 = table_lelang_sk::select('id_user_lowest_lsk','id_lsk')
                    ->get();
                if(isset($data) && count($data) > 0){
                    foreach ($data as $dat) {
                        foreach ($data2->where('id_lsk','=',$dat->id_lsk) as $dat2) {
                            $post[] = [

                                'id_lsk' => $dat->id_lsk,
                                'nama_lsk' => $dat->nama_lsk,
                                'photo_lsk' => $dat->photo_lsk,
                                'no_aggr_lsk' => $dat->no_aggr_lsk,
                                'jumlah_bto_lsk' => $dat->jumlah_bto_lsk,
                                'lowest_bto_lsk' => $dat->lowest_bto_lsk,
                                'deskripsi_lsk' => $dat->deskripsi_lsk,
                                'start_date_lsk' => $dat->start_date_lsk,
                                'end_date_lsk' => $dat->end_date_lsk,
                                'flag_pemenang_dlsk' => $dat->flag_pemenang_dlsk,
                                'id_pemenang' => $dat2->id_user_lowest_lsk,


                            ];
                        }
                    }
                    table_log::insert(array(
                        'ws_name' => 'getPemenangLSK.php',
                        'message_log' => 'Data Sesuai',
                        'created_at' => $date,
                        'id_user' => $id,
                    ));
                    return $this->sendResponse7($post, 'Log Berhasil Di-input / Data Sesuai');
                }
                elseif (isset($data)&& count($data) == 0){
                    table_log::insert(array(
                        'ws_name' => 'getPemenangLSK.php',
                        'message_log' => 'Data Sesuai',
                        'created_at' => $date,
                        'id_user' => $id,
                    ));
                    return $this->sendResponse7($data, 'Log Berhasil Di-input / Data Sesuai');
                }
                else{
                    table_log::insert(array(
                        'ws_name' => 'getPemenangLSK.php',
                        'message_log' => 'Data Tidak Sesuai',
                        'created_at' => $date,
                        'id_user' => $id,
                    ));
                    return $this->sendError('Log Berhasil Di-input / Data Tidak Sesuai');
                }
            }
        }
        else{
            table_log::insert(array(
                'ws_name' => 'getPemenangLSK.php',
                'message_log' => 'Cek gagal',
                'created_at' => $date,
                'id_user' => '0',
            ));
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }
}