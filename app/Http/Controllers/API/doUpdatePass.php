<?php


namespace App\Http\Controllers\API;


use App\table_api_key;
use App\table_log;
use App\table_user;
use Illuminate\Http\Request;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use Validator;
use Hash;



class doUpdatePass extends APIBaseController
{
    public function Store(Request $request)
    {
        $date = date('Y-m-d H:m:s');
        if (table_api_key::where('api_key', '=', $request->get('apiKey'))->exists()) {
            $input = $request->all();
            $validator = Validator::make($input, [
                'password' => 'required',
                'sandi_lama' => 'required'
            ]);
            $id_user = $request->get('id_user');
            if ($validator->fails()) {
                table_log::insert(array(
                    'ws_name' => 'doUpdatePass.php',
                    'message_log' => 'Update Password User Gagal',
                    'created_at' => $date,
                    'id_user' => $id_user,
                ));
                return $this->sendError('Validation Error.', $validator->errors());
            }
            else {

                $post = table_user::find($id_user);
                if(! Hash::check( $request->get('sandi_lama'),$post->password  )) {
                    return $this->sendError('Password lama salah');
                }
                else{
                    $post->password = bcrypt($input['password']);
                    $post->save();

                    if ($post->save()) {
                        table_log::insert(array(
                            'ws_name' => 'doUpdatePass.php',
                            'message_log' => 'Update Password User Berhasil',
                            'created_at' => $date,
                            'id_user' => $id_user,
                        ));
                        return $this->sendResponse1('Update Password User Berhasil');
                    } else {
                        table_log::insert(array(
                            'ws_name' => 'doUpdatePass.php',
                            'message_log' => 'Update Password User Gagal',
                            'created_at' => $date,
                            'id_user' => $id_user,
                        ));
                        return $this->sendError('Update Password User Gagal');
                    }
                }
            }
        }
        else{
            table_log::insert(array(
                'ws_name' => 'doUpdatePass.php',
                'message_log' => 'Update Password User Gagal',
                'created_at' => $date,
                'id_user' => '0',
            ));
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }


}