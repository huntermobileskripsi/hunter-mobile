<?php


namespace App\Http\Controllers\API;


use App\table_api_key;
use App\table_log;
use App\table_user;
use Illuminate\Http\Request;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use Illuminate\Support\Facades\Auth;
use Validator;
use Hash;



class logMain extends APIBaseController
{
    public function log(Request $request)
    {
        $id_user = Auth::id();
        $date = date('Y-m-d H:m:s');
        if (table_api_key::where('api_key', '=', $request->get('apiKey'))->exists()) {
            $validator = Validator::make($request->all(), [
                'id_user' => 'required',
                'message_log' => 'required'

            ]);
            if ($validator->fails()) {
                $log = $request->get('message_log');
                $id = $request->get('id_user');
                table_log::insert(array(
                    'ws_name' => 'logMain.php',
                    'message_log' => $log,
                    'created_at' => $date,
                    'id_user' => $id,
                ));
                return response()->json(['error' => $validator->errors()], 401);
            }
            else {
               $log = $request->get('message_log');
               $id = $request->get('id_user');
               $data = table_log::insert(array(
                    'ws_name' => 'logMain.php',
                    'message_log' => $log,
                    'created_at' => $date,
                    'id_user' => $id,
                ));
                if($data) {
                    return $this->sendResponse1('Log berhasil Di-input');
                }
                else{
                    return $this->sendError('Log gagal Di-input');
                }
            }
        }
        else{
            table_log::insert(array(
                'ws_name' => 'logMain.php',
                'message_log' => 'Cek gagal',
                'created_at' => $date,
                'id_user' => '0',
            ));
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }
}