<?php


namespace App\Http\Controllers\API;


use App\table_api_key;
use App\table_laporan;
use App\table_log;
use App\table_suratkuasa;
use App\table_user;
use Illuminate\Http\Request;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use Validator;
use Hash;



class doPerpanjanganSK extends APIBaseController
{
    public function Store(Request $request)
    {
        $date = date('Y-m-d H:m:s');
        if (table_api_key::where('api_key', '=', $request->get('apiKey'))->exists()) {
            $input = $request->all();
            $validator = Validator::make($input, [
                'id_user' => 'required',
                'id_suratkuasa' => 'required',
            ]);
            $id_user = $request->get('id_user');
            if ($validator->fails()) {
                table_log::insert(array(
                    'ws_name' => 'doPerpanjanganSK.php',
                    'message_log' => 'Data Tidak Sesuai',
                    'created_at' => $date,
                    'id_user' => $id_user,
                ));
                return $this->sendError('Validation Error.', $validator->errors());
            }
            else {
                $idsuratkuasa = $request->get('id_suratkuasa');
                $post = table_suratkuasa::find($idsuratkuasa);
                //$post->flag_perpanjangan = 'N';
                $post->updated_at_p = $date;
                $post->created_at_p = $date;
                $post->created_by_p = $id_user;
                $post->updated_by_p = $id_user;
                $post->save();

                if ($post->save()){
                    $btmk = table_suratkuasa::join('table_btmk','table_suratkuasa.id_btmk','=','table_btmk.id_btmk')
                        ->select('table_suratkuasa.id_suratkuasa','table_suratkuasa.id_btmk','table_btmk.NO_CAR_POLICE',
                            'table_suratkuasa.desc_sp','table_suratkuasa.area','table_suratkuasa.bto_request')
                        ->where('table_suratkuasa.id_suratkuasa','=', $idsuratkuasa)
                        ->first();
                    $data['id_suratkuasa'] = $btmk->id_suratkuasa;
                    $data['id_btmk'] = $btmk->id_btmk;
                    $data['desc_sp'] = $btmk->desc_sp;
                    $data['area'] = $btmk->area;
                    $data['bto_request'] = $btmk->bto_request;

                    table_log::insert(array(
                        'ws_name' => 'doPerpanjanganSK.php',
                        'message_log' => 'Data Sesuai',
                        'created_at' => $date,
                        'id_user' => $id_user,
                    ));

                    return $this->sendResponse6($data,'Log Berhasil Di-input / Data Sesuai');
                }
                else{
                    table_log::insert(array(
                        'ws_name' => 'doPerpanjanganSK.php',
                        'message_log' => 'Data Tidak Sesuai',
                        'created_at' => $date,
                        'id_user' => $id_user,
                    ));

                    return $this->sendError('Log Berhasil Di-input / Data Tidak Sesuai');
                }

            }
        }
        else{
            table_log::insert(array(
                'ws_name' => 'doPerpanjanganSK.php',
                'message_log' => 'Data Gagal Disimpan',
                'created_at' => $date,
                'id_user' => '0',
            ));
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }


}