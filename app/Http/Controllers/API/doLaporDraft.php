<?php


namespace App\Http\Controllers\API;


use App\table_api_key;
use App\table_laporan;
use App\table_log;
use App\table_suratkuasa;
use App\table_user;
use Illuminate\Http\Request;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use Validator;
use Hash;



class doLaporDraft extends APIBaseController
{
    public function Store(Request $request)
    {
        $date = date('Y-m-d H:m:s');
        if (table_api_key::where('api_key', '=', $request->get('apiKey'))->exists()) {
            $input = $request->all();
            $validator = Validator::make($input, [
                'id_user' => 'required',
                'id_lapor' => 'required',
                'flag_draft' => 'required',
            ]);
            $id_user = $request->get('id_user');
            if ($validator->fails()) {
                table_log::insert(array(
                    'ws_name' => 'doLaporDraft.php',
                    'message_log' => 'Data Gagal Disimpan',
                    'created_at' => $date,
                    'id_user' => $id_user,
                ));
                return $this->sendError('Validation Error.', $validator->errors());
            }
            else {
                $idlapor = $request->get('id_lapor');
                $btmk = table_laporan::join('table_btmk','table_laporan.id_btmk','=','table_btmk.id_btmk')
                    ->select('table_btmk.DESC_SP','table_btmk.AREA2','table_btmk.id_btmk')
                    ->where('table_laporan.id_lapor','=', $idlapor)
                    ->first();
                $post = table_laporan::find($idlapor);
                $post->flag_draft = $input['flag_draft'];
                $post->save();

                if($post->save()) {
                    $appr = table_suratkuasa::where('id_btmk','=',$btmk->id_btmk)
                        ->first();
                    if ($appr ==  null) {
                        $data['id_lapor'] = $post->id_lapor;
                        $data['id_btmk'] = $post->id_btmk;
                        $data['desc_sp'] = $btmk->DESC_SP;
                        $data['area'] = $btmk->AREA2;
                        $data['minta_sk'] = 'true';

                        table_log::insert(array(
                            'ws_name' => 'doLaporNow.php',
                            'message_log' => 'Data Berhasil Disimpan',
                            'created_at' => $date,
                            'id_user' => $id_user,
                        ));
                        return $this->sendResponse3($data, 'Log Berhasil Di-input / Data Sesuai');
                    }
                    else {
                        if ($appr->flag_approved == 'N') {
                            $data['id_lapor'] = $post->id_lapor;
                            $data['id_btmk'] = $post->id_btmk;
                            $data['desc_sp'] = $btmk->DESC_SP;
                            $data['area'] = $btmk->AREA2;
                            $data['minta_sk'] = 'true';

                            table_log::insert(array(
                                'ws_name' => 'doLaporDraft.php',
                                'message_log' => 'Data Berhasil Disimpan',
                                'created_at' => $date,
                                'id_user' => $id_user,
                            ));

                            return $this->sendResponse3($data, 'Log Berhasil Di-input / Data Sesuai');
                        } elseif ($appr->flag_approved == 'Y') {
                            if ($appr->end_approved_at < $date) {
                                $data['id_lapor'] = $post->id_lapor;
                                $data['id_btmk'] = $post->id_btmk;
                                $data['desc_sp'] = $btmk->DESC_SP;
                                $data['area'] = $btmk->AREA2;
                                $data['minta_sk'] = 'true';

                                table_log::insert(array(
                                    'ws_name' => 'doLaporDraft.php',
                                    'message_log' => 'Data Berhasil Disimpan',
                                    'created_at' => $date,
                                    'id_user' => $id_user,
                                ));

                                return $this->sendResponse3($data, 'Log Berhasil Di-input / Data Sesuai');
                            } elseif ($appr->flag_perpanjangan == 'N') {
                                $data['id_lapor'] = $post->id_lapor;
                                $data['id_btmk'] = $post->id_btmk;
                                $data['desc_sp'] = $btmk->DESC_SP;
                                $data['area'] = $btmk->AREA2;
                                $data['minta_sk'] = 'true';

                                table_log::insert(array(
                                    'ws_name' => 'doLaporDraft.php',
                                    'message_log' => 'Data Berhasil Disimpan',
                                    'created_at' => $date,
                                    'id_user' => $id_user,
                                ));

                                return $this->sendResponse3($data, 'Log Berhasil Di-input / Data Sesuai');
                            } elseif ($appr->end_perpanjangan_approved_at < $date) {
                                $data['id_lapor'] = $post->id_lapor;
                                $data['id_btmk'] = $post->id_btmk;
                                $data['desc_sp'] = $btmk->DESC_SP;
                                $data['area'] = $btmk->AREA2;
                                $data['minta_sk'] = 'true';

                                table_log::insert(array(
                                    'ws_name' => 'doLaporDraft.php',
                                    'message_log' => 'Data Berhasil Disimpan',
                                    'created_at' => $date,
                                    'id_user' => $id_user,
                                ));

                                return $this->sendResponse3($data, 'Log Berhasil Di-input / Data Sesuai');
                            } else {
                                $data['id_lapor'] = $post->id_lapor;
                                $data['id_btmk'] = $post->id_btmk;
                                $data['desc_sp'] = $btmk->DESC_SP;
                                $data['area'] = $btmk->AREA2;
                                $data['minta_sk'] = 'false';

                                table_log::insert(array(
                                    'ws_name' => 'doLaporDraft.php',
                                    'message_log' => 'Data Berhasil Disimpan',
                                    'created_at' => $date,
                                    'id_user' => $id_user,
                                ));

                                return $this->sendResponse3($data, 'Log Berhasil Di-input / Data Sesuai');
                            }
                        } else {
                            $data['id_lapor'] = $post->id_lapor;
                            $data['id_btmk'] = $post->id_btmk;
                            $data['desc_sp'] = $btmk->DESC_SP;
                            $data['area'] = $btmk->AREA2;
                            $data['minta_sk'] = 'false';

                            table_log::insert(array(
                                'ws_name' => 'doLaporDraft.php',
                                'message_log' => 'Data Berhasil Disimpan',
                                'created_at' => $date,
                                'id_user' => $id_user,
                            ));

                            return $this->sendResponse3($data, 'Log Berhasil Di-input / Data Sesuai');
                        }
                    }
                }
                else{
                    table_log::insert(array(
                        'ws_name' => 'doLaporDraft.php',
                        'message_log' => 'Data Gagal Disimpan',
                        'created_at' => $date,
                        'id_user' => $id_user,
                    ));
                    return $this->sendError('Log Berhasil Di-input / Data Gagal Disimpan');
                }
            }
        }
        else{
            table_log::insert(array(
                'ws_name' => 'doLaporDraft.php',
                'message_log' => 'Data Gagal Disimpan',
                'created_at' => $date,
                'id_user' => '0',
            ));
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }


}