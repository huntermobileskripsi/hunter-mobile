<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;

class table_suratkuasa extends Model
{
    use Authenticatable;
    protected $table = 'table_suratkuasa';
    protected $primaryKey = 'id_suratkuasa';
}