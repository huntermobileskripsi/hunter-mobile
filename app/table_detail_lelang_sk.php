<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;

class table_detail_lelang_sk extends Model
{
    use Authenticatable;
    protected $table = 'table_detail_lelang_sk';
    protected $primaryKey = 'id_detail_lsk';
    protected $fillable = ['id_lsk','id_reg_mitra_dlsk'];

}
