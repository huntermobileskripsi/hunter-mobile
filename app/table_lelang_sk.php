<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;

class table_lelang_sk extends Model
{
    use Authenticatable;
    protected $table = 'table_lelang_sk';
    protected $primaryKey = 'id_lsk';
    public $fillable = ['id_lsk','nama_lsk','photo_lsk','no_aggr_lsk','jumlah_bto_lsk','jumlah_reg_mitra_lsk',
        'deskripsi_lsk','start_date_lsk','end_date_lsk','lowest_bto_lsk','id_user_lowest_lsk'];
}
