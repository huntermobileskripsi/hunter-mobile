<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;

class table_log extends Model
{
    use Authenticatable;
    protected $table = 'table_log';
    protected $primaryKey = 'id';
    //protected $fillable = ['status', 'area2'];
}