<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;

class table_bid_lsk extends Model
{
    use Authenticatable;
    protected $table = 'table_bid_lsk';
    protected $primaryKey = 'id_bid_lsk';
    protected $fillable = ['id_bid_lsk','id_detail_lsk','id_reg_mitra_dlsk','jumlah_bid_dlsk','created_at','created_by',
        'is_deleted'];
}