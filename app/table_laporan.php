<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;

class table_laporan extends Model
{
    use Authenticatable;
    protected $table = 'table_laporan';
    protected $primaryKey = 'id_lapor';
    protected $fillable = ['no_polisi','pict_tumbnail','location','desc_location','desc_unit','dt_added','user_added',
        'id_user','status','pict_tumbnail2','nama_lengkap','data_cabang','data_area','area2','cd_sp_area', 'no_chasis'];
}