<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Route::get('admin', array('uses' => 'HomeController@showLogin'));
//Route::post('login',array('uses' => 'HomeController@doLogin'));
//Route::get('logout', array('as' => 'keluar', 'uses' => 'HomeController@doLogout'));
//
//Route::get('list', array('before' => 'auth', 'as' => 'list', 'uses' => 'UnitController@viewdata'));
//Route::get('listsearch', array('before' => 'auth', 'as' => 'list.search', 'uses' => 'UnitController@search'));
//
//Route::get('dash', array('before' => 'auth', 'uses' => 'dashController@index'));
//
//Route::get('appr', array('before' => 'auth', 'as' => 'appr', 'uses' => 'ApprController@viewdata'));
//
//Route::get('update/{id_lapor}/view', array('before' => 'auth', 'as' => 'update.appr', 'uses' => 'ApprController@update'));
//Route::post('update/{id_lapor}/update', array('before' => 'auth', 'as' => 'updateappr.appr','uses' => 'ApprController@update1'));
//Route::get('viewappr/{id_lapor}/view', array('before' => 'auth', 'as' => 'view.appr', 'uses' => 'ApprController@viewdata2'));
//
//Route::get('imfile',array('before' => 'auth','as'=>'excel.import','uses'=>'ImfileController@importExportExcelORCSV'));
//Route::post('imfile',array('before' => 'auth','as'=>'import-csv-excel','uses'=>'ImfileController@importFileIntoDB'));
//Route::post('exlap/{type}',array('before' => 'auth', 'as' => 'export.file', 'uses' => 'ExlapController@exportFile'));
//Route::get('exlap', array('before' => 'auth', 'uses' => 'ExlapController@showExlap'));
//
//Route::get('point', array('before' => 'auth', 'uses' => 'PointController@viewdata'));
//Route::post('point', array('before' => 'auth', 'uses' => 'PointController@insert'));
//Route::get('addpoint', array('before' => 'auth', 'uses' => 'PointController@viewdata2'));
//Route::get('updatepoint/{id_point}/view', array('before' => 'auth', 'as' => 'updatepoint.point','uses' => 'PointController@viewdata3'));
//Route::post('updatepoint/{id_point}/update', array('before' => 'auth', 'as' => 'Point.Update','uses' => 'PointController@update'));
//Route::post('point/{id_point}',array('before' => 'auth', 'as' => 'deletepoint.point', 'uses' => 'PointController@delete'));
//
//Route::get('reward', array('before' => 'auth', 'uses' => 'RewardController@viewdata'));
//Route::post('reward', array('before' => 'auth', 'uses' => 'RewardController@insert'));
//Route::get('addreward', array('before' => 'auth', 'uses' => 'RewardController@viewdata2'));
//Route::get('updatereward/{id_reward}/view', array('before' => 'auth', 'as' => 'updatereward.reward','uses' => 'RewardController@viewdata3'));
//Route::post('updatereward/{id_reward}/update', array('before' => 'auth', 'as' => 'Reward.Update','uses' => 'RewardController@update'));;
//Route::post('reward/{id_reward}',array('before' => 'auth', 'as' => 'deletereward.reward', 'uses' => 'RewardController@delete'));
//
//Route::get('lelangsk', array('before' => 'auth', 'uses' => 'SkController@viewdata'));
//Route::post('checksk', array('before' => 'auth', 'as' => 'sk.check', 'uses' => 'SkController@checknoaggr'));
//Route::post('addmitra/{id_lsk}/update', array('before' => 'auth', 'as' => 'addmitra.lsk', 'uses' => 'SkController@addmitra'));
//Route::get('addlelangsk/view/{noagg}', array('before' => 'auth', 'as' => 'sk.add', 'uses' => 'SkController@viewdata2'));
//Route::post('lelangsk', array('before' => 'auth', 'uses' => 'SkController@insert'));
//Route::post('updatelelang/{id_lsk}/update', array('before' => 'auth', 'as' => 'updatelelang', 'uses' => 'SkController@update'));
//Route::get('updatelelang/{id_lsk}/view', array('before' => 'auth', 'as' => 'updatelelang.view', 'uses' => 'SkController@viewdata4'));
//Route::get('viewlelangsk/{id_lsk}/view', array('before' => 'auth', 'as' => 'view.lelang', 'uses' => 'SkController@viewdata3'));
//Route::post('lelangsk/{id_lsk}',array('before' => 'auth', 'as' => 'deletesk.sk', 'uses' => 'SkController@delete'));
//
//Route::get('sayemsk', array('before' => 'auth', 'uses' => 'SayembaraController@viewdata'));
//Route::post('sayemchecksk', array('before' => 'auth', 'as' => 'sayemsk.check', 'uses' => 'SayembaraController@checknoaggr'));
//Route::get('addsayemsk/view/{noagg}', array('before' => 'auth', 'as' => 'sayemsk.add', 'uses' => 'SayembaraController@viewdata2'));
//Route::post('sayemsk', array('before' => 'auth', 'uses' => 'SayembaraController@insert'));
//Route::post('sayemsk/{id_ssk}',array('before' => 'auth', 'as' => 'deletessk.ssk', 'uses' => 'SayembaraController@delete'));
//Route::post('addmitrassk/{id_ssk}/update', array('before' => 'auth', 'as' => 'addmitra.ssk', 'uses' => 'SayembaraController@addmitra'));
//Route::post('updatesayem/{id_ssk}/update', array('before' => 'auth', 'as' => 'updatesayem', 'uses' => 'SayembaraController@update'));
//Route::get('updatesayem/{id_ssk}/view', array('before' => 'auth', 'as' => 'updatesayem.view', 'uses' => 'SayembaraController@viewdata4'));
//Route::get('viewsayemsk/{id_ssk}/view', array('before' => 'auth', 'as' => 'view.sayem', 'uses' => 'SayembaraController@viewdata3'));
//
//Route::get('reqsk', array('before' => 'auth', 'uses' => 'RequestSkController@viewdata'));
Route::get('/', function() {return view('welcome');} );
@include('api');
// Route::group(['middleware' => 'disablepreventback'],function(){
    // Auth::routes();
    // /* Route::get('admin', array('uses' => 'HomeController@showLogin'));
    // Route::post('login',array('uses' => 'HomeController@doLogin'));
    // Route::get('logout', array('as' => 'keluar', 'uses' => 'HomeController@doLogout'));
 // */
    // Route::get('list', array('before' => 'auth', 'as' => 'list', 'uses' => 'UnitController@index'));
    // Route::get('listsearch', array('before' => 'auth', 'as' => 'list.search', 'uses' => 'UnitController@search'));

    // Route::get('dash', array('before' => 'auth', 'uses' => 'dashController@index'));

    // Route::get('appr', array('before' => 'auth', 'as' => 'appr', 'uses' => 'ApprController@viewdata'));

    // Route::get('update/{id_lapor}/view', array('before' => 'auth', 'as' => 'update.appr', 'uses' => 'ApprController@update'));
    // Route::post('update/{id_lapor}/update', array('before' => 'auth', 'as' => 'updateappr.appr','uses' => 'ApprController@update1'));
    // Route::get('viewappr/{id_lapor}/view', array('before' => 'auth', 'as' => 'view.appr', 'uses' => 'ApprController@viewdata2'));

   // Route::get('imfile',array('before' => 'auth','as'=>'excel.import','uses'=>'ImfileController@importExportExcelORCSV'));
    // Route::post('imfile',array('before' => 'auth','as'=>'import-csv-excel','uses'=>'UnitController@importFileIntoDB'));
    // Route::post('exlap/{type}',array('before' => 'auth', 'as' => 'export.file', 'uses' => 'ExlapController@exportFile'));
    // Route::get('exlap', array('before' => 'auth', 'uses' => 'ExlapController@showExlap'));
    // Route::post('exsk/{type}',array('before' => 'auth', 'as' => 'export.sk', 'uses' => 'ExskController@exportFile'));
    // Route::get('exsk', array('before' => 'auth', 'uses' => 'ExskController@showExlap'));

   // Route::get('point', array('before' => 'auth', 'uses' => 'PointController@viewdata'));
   // Route::post('point', array('before' => 'auth', 'uses' => 'PointController@insert'));
   // Route::get('addpoint', array('before' => 'auth', 'uses' => 'PointController@viewdata2'));
   // Route::get('updatepoint/{id_point}/view', array('before' => 'auth', 'as' => 'updatepoint.point','uses' => 'PointController@viewdata3'));
   // Route::post('updatepoint/{id_point}/update', array('before' => 'auth', 'as' => 'Point.Update','uses' => 'PointController@update'));
   // Route::post('point/{id_point}',array('before' => 'auth', 'as' => 'deletepoint.point', 'uses' => 'PointController@delete'));

   // Route::get('reward', array('before' => 'auth', 'uses' => 'RewardController@viewdata'));
   // Route::post('reward', array('before' => 'auth', 'uses' => 'RewardController@insert'));
   // Route::get('addreward', array('before' => 'auth', 'uses' => 'RewardController@viewdata2'));
   // Route::get('updatereward/{id_reward}/view', array('before' => 'auth', 'as' => 'updatereward.reward','uses' => 'RewardController@viewdata3'));
   // Route::post('updatereward/{id_reward}/update', array('before' => 'auth', 'as' => 'Reward.Update','uses' => 'RewardController@update'));;
   // Route::post('reward/{id_reward}',array('before' => 'auth', 'as' => 'deletereward.reward', 'uses' => 'RewardController@delete'));

    // Route::get('lelangsk', array('before' => 'auth', 'uses' => 'SkController@viewdata'));
    // Route::post('checksk', array('before' => 'auth', 'as' => 'sk.check', 'uses' => 'SkController@checknoaggr'));
    // Route::post('addmitra/{id_lsk}/update', array('before' => 'auth', 'as' => 'addmitra.lsk', 'uses' => 'SkController@addmitra'));
    // Route::get('addlelangsk/view/{noagg}', array('before' => 'auth', 'as' => 'sk.add', 'uses' => 'SkController@viewdata2'));
    // Route::post('lelangsk', array('before' => 'auth', 'uses' => 'SkController@insert'));
    // Route::post('updatelelang/{id_lsk}/update', array('before' => 'auth', 'as' => 'updatelelang', 'uses' => 'SkController@update'));
    // Route::get('updatelelang/{id_lsk}/view', array('before' => 'auth', 'as' => 'updatelelang.view', 'uses' => 'SkController@viewdata4'));
    // Route::get('viewlelangsk/{id_lsk}/view', array('before' => 'auth', 'as' => 'view.lelang', 'uses' => 'SkController@viewdata3'));
    // Route::post('lelangsk/{id_lsk}',array('before' => 'auth', 'as' => 'deletesk.sk', 'uses' => 'SkController@delete'));
    // Route::post('updatepemenang/{id_lsk}/update', array('before' => 'auth', 'as' => 'update.pemenang', 'uses' => 'SkController@pemenang'));
    // Route::post('batal/{id_lsk}/',array('before' => 'auth', 'as' => 'batal.sk', 'uses' => 'SkController@batal'));

   // Route::get('sayemsk', array('before' => 'auth', 'uses' => 'SayembaraController@viewdata'));
   // Route::post('sayemchecksk', array('before' => 'auth', 'as' => 'sayemsk.check', 'uses' => 'SayembaraController@checknoaggr'));
   // Route::get('addsayemsk/view/{noagg}', array('before' => 'auth', 'as' => 'sayemsk.add', 'uses' => 'SayembaraController@viewdata2'));
   // Route::post('sayemsk', array('before' => 'auth', 'uses' => 'SayembaraController@insert'));
   // Route::post('sayemsk/{id_ssk}',array('before' => 'auth', 'as' => 'deletessk.ssk', 'uses' => 'SayembaraController@delete'));
   // Route::post('addmitrassk/{id_ssk}/update', array('before' => 'auth', 'as' => 'addmitra.ssk', 'uses' => 'SayembaraController@addmitra'));
   // Route::post('updatesayem/{id_ssk}/update', array('before' => 'auth', 'as' => 'updatesayem', 'uses' => 'SayembaraController@update'));
   // Route::get('updatesayem/{id_ssk}/view', array('before' => 'auth', 'as' => 'updatesayem.view', 'uses' => 'SayembaraController@viewdata4'));
   // Route::get('viewsayemsk/{id_ssk}/view', array('before' => 'auth', 'as' => 'view.sayem', 'uses' => 'SayembaraController@viewdata3'));

    // Route::get('reqsk', array('before' => 'auth', 'uses' => 'RequestSkController@viewdata'));

    // Route::get('usermanage', array('before' => 'auth', 'uses' => 'UserManageController@viewdata'));
    // Route::post('adduser', array('before' => 'auth', 'uses' => 'UserManageController@insert'));
    // Route::post('updateuser/{id_user}/update', array('before' => 'auth', 'as' => 'update.user', 'uses' => 'UserManageController@update'));

    // Route::get('permintaansk', array('before' => 'auth', 'uses' => 'PermintaanSkController@viewdata'));
    // Route::get('viewpermintaan/{id_suratkuasa}/view', array('before' => 'auth', 'as' => 'view.permintaan', 'uses' => 'PermintaanSkController@viewdata2'));
    // Route::post('updatepermintaan/{id_suratkuasa}/update', array('before' => 'auth', 'as' => 'updatepermintaan', 'uses' => 'PermintaanSkController@update'));
    // Route::get('reject/{id_suratkuasa}/update', array('before' => 'auth', 'as' => 'reject', 'uses' => 'PermintaanSkController@reject'));

    // Route::get('dashboardsk', array('before' => 'auth', 'uses' => 'DashboardSkController@viewdata'));
    // Route::get('viewdashboard/{id_suratkuasa}/view', array('before' => 'auth', 'as' => 'view.dashboard', 'uses' => 'DashboardSkController@viewdata2'));

    // Route::get('perpanjangansk', array('before' => 'auth', 'uses' => 'PerpanjanganSkController@viewdata'));
    // Route::get('viewperpanjangan/{id_suratkuasa}/view', array('before' => 'auth', 'as' => 'view.perpanjangan', 'uses' => 'PerpanjanganSkController@viewdata2'));
    // Route::post('updateperpanjangan/{id_suratkuasa}/update', array('before' => 'auth', 'as' => 'updateperpanjangan', 'uses' => 'PerpanjanganSkController@update'));
    // Route::get('rejectperpanjangan/{id_suratkuasa}/update', array('before' => 'auth', 'as' => 'rejectperpanjangan', 'uses' => 'PerpanjanganSkController@reject'));
// }); 

