<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/;


Route::post('dologin', 'API\doLogIn@login');
Route::post('dologout', 'API\doLogOut@logout');
Route::post('dodaftar', 'API\doDaftar@register');
Route::get('getceklupakatasandi', 'API\getCekLupaKataSandi@index');
Route::post('doperbaruikatasandi', 'API\doPerbaruiKataSandi@store');
Route::post('doaktivasiakun', 'API\doAktivasiAkun@store');
Route::post('doresendotpaktivasi', 'API\doResendOtpAktivasi@index');
Route::post('doresendotppass', 'API\doResendOtpPass@index');
Route::post('docekotppass', 'API\doCekOtpPass@store');
Route::get('getalltablelog', 'API\GetAllTableLog@index');
Route::get('getalltableuser', 'API\GetAllTableUser@index');
//Route::get('getallbtmk', 'API\getallbtmk@index');
//Route::get('getalllaporan', 'API\getalllaporan@index');
//Route::get('getalldraft', 'API\getalldraft@index');

Route::group(['middleware' => 'auth:api'], function(){
    Route::post('dologout', 'API\doLogOut@logout');
    Route::post('logmain', 'API\logMain@log');
    Route::post('doupdatedata', 'API\doUpdateData@store');
    Route::post('doupdatepass', 'API\doUpdatePass@store');
    Route::post('dolapornow', 'API\doLaporNow@store');
    Route::post('dosimpankedraft', 'API\doSimpanKeDraft@store');
    Route::post('doperbaruidraftsaya', 'API\doPerbaruiDraftSaya@store');
    Route::post('dolapordraft', 'API\doLaporDraft@store');
    Route::post('domintask', 'API\doMintaSK@store');
    Route::post('doperpanjangansk', 'API\doPerpanjanganSK@store');
    Route::post('dotawarlsk', 'API\doTawarLSK@store');
    Route::get('getallaktifsk', 'API\getAllAktifSK@log');
    Route::get('getriwayat', 'API\getRiwayat@log');
    Route::get('getdraftsaya', 'API\getDraftSaya@log');
    Route::get('carikendaraan', 'API\cariKendaraan@index');
    Route::get('getskrequestsaya', 'API\getSKRequestSaya@log');
    Route::get('getskaktifsaya', 'API\getSKAktifSaya@log');
    Route::get('getLelangSK', 'API\getLelangSK@log');
    Route::get('getpemenanglsk', 'API\getPemenangLSK@log');
});

